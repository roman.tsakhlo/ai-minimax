// basic variables

const board = [
  ["", "", ""],
  ["", "", ""],
  ["", "", ""],
];

const statusGame = {
  X: "X",
  O: "O",
  lastStep: "lastStep",
};

const options = {
  cross: statusGame.X,
  circle: statusGame.O,
};

let choosedOption = "";

const cells = document.querySelectorAll(".cell");
const optionX = document.getElementById("cross");
const optionO = document.getElementById("circle");

// update text information for player

function updateStatusGame(winner) {
  let text = document.querySelector("h2");
  text.textContent = "";

  if (winner === "lastStep") {
    text.append("Reload page and try again");
  } else {
    text.append(`The winner is ${winner}`);
    let id = winner === options.cross ? "cross" : "circle";
    let winnerDiv = document.getElementById(id);
    let choosedDiv = document.querySelector(".choosed-option");
    choosedDiv?.classList.remove("choosed-option");
    winnerDiv?.classList.add("winner-option");
  }
}

function updateStepStatus(message) {
  document.getElementById("step_status").textContent = message;
}

// board and cells:

function getBoard() {
  const board = [];
  const cells = document.querySelectorAll(".cell");

  for (let i = 0; i < 3; i++) {
    const row = [];
    for (let j = 0; j < 3; j++) {
      const cell = cells[i * 3 + j];

      let option = cell.dataset.cross
        ? cell.dataset.cross
        : cell.dataset.circle
        ? cell.dataset.circle
        : "";

      row.push(option);
    }
    board.push(row);
  }

  return board;
}

cells.forEach((cell) => {
  cell.addEventListener("click", function () {
    let row = this.dataset.row;
    let col = this.dataset.col;
    moving(row, col, choosedOption);
  });
});

// choose option for play: X or O

function chooseOption(optionDiv, opositeOptionDiv, option) {
  optionDiv.addEventListener("click", function () {
    optionDiv.classList.add("choosed-option");
    opositeOptionDiv.classList.add("disabled-option");
    opositeOptionDiv.disabled = true;

    choosedOption = options.cross === option ? statusGame.X : statusGame.O;
  });
}

optionX.addEventListener(
  "click",
  chooseOption(optionX, optionO, options.cross)
);

optionO.addEventListener(
  "click",
  chooseOption(optionO, optionX, options.circle)
);

// do move

function moving(row, col, currentOption) {
  const cell = document.querySelector(`[data-row="${row}"][data-col="${col}"]`);

  if (!cell.innerHTML && choosedOption !== "") {
    if (!cell.innerHTML) {
      if (currentOption === options.cross) {
        cell.innerHTML = document.getElementById("cross").innerHTML;
        cell.dataset.cross = "X";
      } else if (currentOption === options.circle) {
        cell.innerHTML = document.getElementById("circle").innerHTML;
        cell.dataset.circle = "O";
      }
    }

    const winner = checkWinner(getBoard());

    if (winner === null) {
      updateStepStatus(
        `${currentOption === choosedOption ? "Computer step" : "Your step"}`
      );
      currentOption =
        currentOption === statusGame.X ? statusGame.O : statusGame.X;

      if (
        (currentOption === statusGame.X && choosedOption === statusGame.O) ||
        (currentOption === statusGame.O && choosedOption === statusGame.X)
      ) {
        const bestMoveForComputer = bestMove(getBoard());
        console.log("bestMoveForComputer", bestMoveForComputer);

        if (bestMoveForComputer) {
          setTimeout(() => {
            moving(bestMoveForComputer.i, bestMoveForComputer.j, currentOption);
          }, 100);
        }
      }
    } else {
      updateStatusGame(winner);
    }
  }
}

function checkWinner(board) {
  for (let i = 0; i < 3; i++) {
    // play finished on row
    if (
      board[i][0] !== "" &&
      board[i][0] === board[i][1] &&
      board[i][0] === board[i][2]
    ) {
      return board[i][0];
    }

    // play finished on column
    if (
      board[0][i] !== "" &&
      board[0][i] === board[1][i] &&
      board[0][i] === board[2][i]
    ) {
      return board[0][i];
    }
  }

  // play finished on diagonals
  if (
    board[0][0] !== "" &&
    board[0][0] === board[1][1] &&
    board[0][0] === board[2][2]
  ) {
    return board[0][0];
  }
  if (
    board[0][2] !== "" &&
    board[0][2] === board[1][1] &&
    board[0][2] === board[2][0]
  ) {
    return board[0][2];
  }

  for (let row = 0; row < 3; row++) {
    for (let col = 0; col < 3; col++) {
      // play continue
      if (board[row][col] === "") {
        return null;
      }
    }
  }

  // play draw
  return "lastStep";
}

function minimax(board, depth, isMaximizingStep) {
  const result = checkWinner(board);

  if (result !== null) {
    if (result === statusGame.X) {
      return 10 - depth;
    } else if (result === statusGame.O) {
      return depth - 10;
    }

    if (result === statusGame.lastStep) {
      return 0;
    }
  }

  if (isMaximizingStep) {
    let bestScore = -Infinity;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (board[i][j] === "") {
          board[i][j] = statusGame.X;
          const score = minimax(board, depth + 1, false);
          board[i][j] = "";
          bestScore = Math.max(score, bestScore);
        }
      }
    }
    return bestScore;
  } else {
    let bestScore = Infinity;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (board[i][j] === "") {
          board[i][j] = statusGame.O;
          const score = minimax(board, depth + 1, true);
          board[i][j] = "";
          bestScore = Math.min(score, bestScore);
        }
      }
    }
    return bestScore;
  }
}

function bestMove(board) {
  let bestScore = -Infinity;
  let move;

  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (board[i][j] === "") {
        board[i][j] = statusGame.X; // ?
        const score = minimax(board, 0, false);
        board[i][j] = "";

        if (score > bestScore) {
          bestScore = score;
          move = { i, j };
        }
      }
    }
  }

  return move;
}
